#!/bin/bash

# ----------------------------------------------------------------------
# script instalación de programas extras en Debian
# CC-BY 2024 Miguel Sevilla-Callejo
# actualizado a 2024-06-04 en Debian 12 Stable / Bookworm con Gnome
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
## AÑADIR REPOSITORIOS EXTRAS ------------------------------------------

## Pendiente de revisar y sustituir por flatpak

apt install -y software-properties-common wget curl

# [1] QGIS latest stable version --> hay versión flatpak
	# descargamos la clave de firma de qgis
wget -O /etc/apt/keyrings/qgis-archive-keyring.gpg https://download.qgis.org/downloads/qgis-archive-keyring.gpg
	# creamos el archivo para el repositorio de QGIS
echo "Types: deb deb-src
URIs: https://qgis.org/debian
Suites: bookworm
Architectures: amd64
Components: main
Signed-By: /etc/apt/keyrings/qgis-archive-keyring.gpg" | tee /etc/apt/sources.list.d/qgis.sources

# [2] VSCodium --> hay versión flatpak
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
    | gpg --dearmor \
    | dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg

echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' \
    | tee /etc/apt/sources.list.d/vscodium.list


# ----------------------------------------------------------------------
# ACTUALIZAR -----------------------------------------------------------
apt update && apt upgrade -y


# ----------------------------------------------------------------------
# INSTALAR -------------------------------------------------------------

# herramientas del sistema ---------------------------------------------
apt install -y zsh		# zsh shell
apt install -y ssh     		# secure shell
apt install -y make cmake	# compilar paquetes
apt install -y cifs-utils	# para montar sistemas smb y win
apt install -y zenity  		# mostrar ventanas y notificaciones
apt install -y rsync		# sincronización local y remota (ssh)
apt install -y gocryptfs cryfs	# encriptar archivos
apt install -y sirikali   		# GUI para montar carpetas encriptadas y remotas
apt install -y rclone \
	rclone-browser 			# accede servicios en la nube
#apt install -y restic            	# backups encriptados y secuenciales
apt install -y borgbackup vorta		# borg backup + vorta (GUI) idem arriba

apt install -y powertop          	# gestión de energía
#apt install -y tlp tlp-rdw        # control del gasto de energia y batería
apt install -y power-profiles-daemon # idem (incompatible con tlp y necesario en Gnome)
apt install -y	cpufrequtils task-laptop upower # más utilidades para portátil
apt install -y dosfstools mtools   # utilidades para discos MS-DOS
apt install -y testdisk            # escaner de particiones y recuperación de archivos
#apt install -y ntpdate		# sincroniza reloj del sistema con externo (e.g. hora.roa.es)
apt install -y htpdate      # daemon que sincroniza reloj del sistema vía http
apt install -y sshuttle		# para crear tunel SSH tipo VPN

apt install -y flatpak		# flatpak --> ver script propio para instalar paquetes
flatpak \
	remote-add --if-not-exists \
	flathub https://flathub.org/repo/flathub.flatpakrepo # repo flatpak para el sistema

apt install -y waypipe # para enviar o traer aplicaciones gráficas wayland de manera remota

#apt install -y nvidia-detect 			# detectar NVIDIA
#apt install -y nvidia-tesla-470-driver # driver nvidia (HP Z640)
apt install -y cifs-utils # para cargar archivos o unidades remotas de windows

apt install -y fail2ban
systemctl enable fail2ban
	

# editores -------------------------------------------------------------
apt install -y vim             	# editor de textos CLI
apt install -y geany            # editor de textos GUI
apt install -y codium			# [2] VSCodium -- ver
apt install -y libreoffice-l10n-es \
	hunspell-es hyphen-es mythes-es		# libreoffice español
apt install -y pandoc			# transformar archivos
apt install -y texlive-latex-recommended   # para trabajar con LaTeX
apt install -y texlive-lang-spanish	# paquete en español
apt install -y texlive-xetex		# necesario para crear PDFs en Quarto
apt install -y texlive-latex-extra \
	texlive-publishers texlive-science # más paquetes textlive (Alessio)
#apt install -y evince			# visor de PDF de gnome / preinstalado
apt install -y calibre calibre-bin	# maneja ebooks + binary plugins
apt install -y meld              	# comparar archivos


# internet -------------------------------------------------------------
apt install -y chromium		# navegador Chromium
#apt install -y thunderbird	# cliente de correo / preinstalado
apt install -y nextcloud-desktop 	# cliente de la nube libre
apt install -y telegram-desktop	# mensajería instantánea
apt install -y remmina \
	remmina-plugin*   		# conexión escritorios remotos
apt install -y traceroute		# para trazar conexión a servidores
apt install -y onionshare		# compartir archivos por red tor
#apt install -y amule		# gestor descargas P2P
#apt install -y transmission-gtk	# gestor de torrents
#apt install -y signal-desktop 	# instalar por flatpak


# diseño ---------------------------------------------------------------
apt install -y inkscape        	# editor vectorial
apt install -y gimp		# edición fotográfica
apt install -y scribus         	# maquetación de docs y edición de PDFs 
apt install -y pdfarranger	# modificar PDFs (mover, quitar o unir páginas)
apt install -y krop             # cortar PDFs
apt install -y flameshot        # capturador de pantalla (disponible en flatpak)
apt install -y peek             # captura de pantalla en vídeo
apt install -y darktable	# revelado digital
apt install -y gucharmap 		# mapa de caracteres
apt install -y fonts-font-awesome	# fuente especial para tema zsh agnostic
apt install -y fonts-powerline		# idem para zsh
apt install -y tesseract-ocr \
	tesseract-ocr-spa		# OCR + lenguaje español
apt install -y ocrfeeder \
	gimagereader			# GUI OCR
#apt install -y gpick		# escoger color NO funciona en wayland (ver extensión Gnome)
apt install -y pdftk			# herramientas para manipular PDFs (Alessio) 


# multimedia -----------------------------------------------------------
apt install -y vlc
#apt install -y spotify-client 	# [4] Spotify --> instalar por flatpak
apt install -y obs-studio		# grabación de pantalla


# otras herramientas CLI -----------------------------------------------
apt install -y byobu            # terminales independientes
apt install -y htop glances     # visor de procesos CLI (tipo top)
apt install -y screenfetch      # visor de parametros del sistema
apt install -y mc               # explorador CLI
apt install -y lshw		# listar hardware
apt install -y yt-dlp           # alternativa a youtube-dl
apt install -y docker.io docker-compose # virtualización ligera
# apt install -y podman		# alternativa a docker (a revisar)		
apt install -y locate           # indexa archivos y luego los localiza
apt install -y sshuttle         # para crear conexión VPN vía SSH
#apt install -y adb fastboot	# herramientas para disp. android
		# scrcpy --> instalar por pip o pipx

# otras herramientas GUI -----------------------------------------------
apt install -y keepassxc        # gestor de contraseñas
apt install -y gparted          # particionar discos GUI
apt install -y menulibre        # editar menú de apps del DE
apt install -y cowsay cmatrix	# aplicaciones divertidas CLI

# juegos ---------------------------------------------------------------
#apt install -y minetest	# da problemas --> instalar por flatpak
#apt install -y supertuxkart	# idem


# tweaks ---------------------------------------------------------------
#apt install -y plank		# no necesario en gnome
apt install -y papirus-icon-theme	# tema de iconos
#apt install -y numix-icon-theme	# tema de iconos

	# i3wm
#apt install -y i3 i3blocks suckless-tools
#apt install -y dmenu i3lock xbacklight feh
#apt install -y nitrogen rofi gucharmap


# análisis de datos / desarrollo -------------------------------------

	# datos espaciales
apt install -y qgis qgis-plugin-grass		# [1] QGIS
apt install -y grass grass-gui grass-doc	# GRASS GIS
apt install -y saga				# SAGA GIS
apt install -y osmium-tool			# maneja archivos OSM

	# R
apt install -y r-base-core r-base-dev
apt install -y r-base-html build-essential
apt install -y r-recommended # revisar si corrigen eso de abajo

	# dependencias necesarias para instalación de algunos paquetes:		# nombrepaquete
	apt install -y make libcurl4-openssl-dev libssl-dev pandoc libxml2-dev	# tydiverse
	apt install -y libmariadb-dev libssl-dev libsodium-dev 			# openssl / libmariadbclient-dev
	apt install -y libgdal-dev libproj-dev build-essential 			# gdal
	apt install -y libudunits2-dev libgdal-dev libgeos-dev libproj-dev	# sf
	apt install -y protobuf-compiler libprotobuf-dev 			# tmap
	apt install -y default-jdk 						# rJava
	apt install -y libfontconfig1-dev libharfbuzz-dev libfribidi-dev	# devtools
	apt install -y libgmp-dev libmpfr-dev 					# HH
	apt install -y libharfbuzz-dev libfribidi-dev 				# textshaping

	# paquetes necesarios para RStudio (y descarga, jq)
	apt install -y libclang-dev jq
	
	# descargar e instalar última versión de RStudio # REVISAR
	
	# Fuente: https://gitlab.com/rspatial_es/general/-/blob/master/install_rcran_ubuntu.sh
	
	# Obtener URL desde el repo https://github.com/Thell/rstudio-latest-urls
	# Ahí se actualizan diariamente los enlaces a últimas versiones 
	# `daily`, `preview` y `stable`
	#url=https://github.com/thell/rstudio-latest-urls/raw/master/latest.json
	#url=$(jq -r '.stable.desktop.bionic.rstudio' <(curl -s -L ${url}))

	# lo anterior no funciona (no hay versión jammy) Se usó la siguiente

	#url="https://download1.rstudio.org/electron/jammy/amd64/rstudio-2023.09.1-494-amd64.deb"
	
	# Descarga de fichero deb e instalación del mismo
	#curl --output /tmp/rstudio.deb $url
	#dpkg -i /tmp/rstudio.deb

	# Python
apt install -y python3
apt install -y python3-pip					# Gestor de paquetes de Python3
apt install -y python3-jupyterlab-server 	# IDE para python
apt install -y python3-ipykernel			# necesario para tratar archivos jupyter
apt install -y python3-pandas				# tratamiento de datos
apt install -y python3-scipy python3-numpy \
	python3-graph-tool python3-img2pdf \
	python3-matplotlib python3-networkx \
	python3-seaborn python3-jsonschema # paquetes python extras (Alessio)

	# Ruby
apt install -y ruby-full


# juegos ---------------------------------------------------------------

#sudo apt install -y minetest			# da problemas --> instalar por flatpak
#sudo apt install -y supertuxkart		# idem


# OTROS -------------------------------------------------------

	# virtualización
apt install -y virt-manager
	# sudo usermod -a -G libvirt $USER # añadir usuario al grupo libvirt


## Cambios necesarios para instalar controladores NVIDIA
# https://linuxconfig.org/how-to-install-nvidia-driver-on-debian-12-bookworm-linux

add-apt-repository contrib non-free-firmware # para tener paquetes no libres (nvidia)
# esto anterior NO funcionó y tuve que cambiar a mano `non-free-firmware` a `non-free`

apt install -y nvidia-detect 			# detectar NVIDIA
#apt install -y nvidia-tesla-470-driver # driver nvidia (HP Z640) -- BUSCAR DRIVERS PRIVATIVOS

apt -y install linux-headers-$(uname -r) build-essential libglvnd-dev pkg-config


## REVISAR DESDE AQUÍ

	# TV & Radio
apt install -y freetuxtv  

	# CLI Spotify client: ncspot --> está en flatpak
	# https://github.com/hrkfdn/ncspot

	# dependencias
apt install -y libncursesw5-dev libdbus-1-dev \
	libpulse-dev libssl-dev libxcb1-dev libxcb-render0-dev \
	libxcb-shape0-dev libxcb-xfixes0-dev
apt install -y cargo	# Gestor de paquetes de Rust
#cargo install ncspot
cd tmp && git clone https://github.com/hrkfdn/ncspot
cd /tmp/ncspot
cargo deb
cd /tmp/ncspot/target/debian
apt install -y ./ncspot_*_amd64.deb


	# CLI Youtube Player: ytfzf 
	# https://github.com/pystardust/ytfzf
	
	# dependencias
apt install -y jq mpv youtube-dl fzf
apt install -y libxext-dev python3.9-dev
#pip3 install ueberzug # X11
apt install -y catimg # Wayland
	# clonar repo e instalar
cd /tmp
git clone https://github.com/pystardust/ytfzf
cd ytfzf
make install

exit


# Flatpak -------------------------------------------------------
# Ver paquetes flatpak en script independiente

# ---------------------------------------------------------------
# Desde aquí correr con usuario

# extensiones útiles oh-my-zsh
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# Extensiones gnome4

# https://extensions.gnome.org/extension/307/dash-to-dock/
# https://extensions.gnome.org/extension/615/appindicator-support/
# https://extensions.gnome.org/extension/1732/gtk-title-bar/

# manual en color
apt install -y most && export PAGER='most'

# Archivos de configuración comunes
# thunderbird, firefox, git, vscode, zotero

# autofirma
# https://firmaelectronica.gob.es/Home/Descargas.html
