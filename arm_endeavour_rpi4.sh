#!/bin/bash

# Paquetes a instalar en versión ARM de EndeavaourOS
# Instalación en Raspberry Pi 4
# Más info e instalación en: https://arm.endeavouros.com/
# actualización a 20220215
# CC BY Miguel Sevilla-Callejo

# Comandos provisionales copiados desde .bash_history de RPi4

yay -S openssh vim byobu 


# internet
yay -S chromium
yay -S widevine-armv7h # tarda en compilar pero funciona
