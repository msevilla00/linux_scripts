#!/bin/bash

##############################################################################################
#                                                                                            #
#  Script de instalación personalizado para Ubuntu 20.04 LTS focal (y derivadas)             #
#                                                                                            #
#  CC-BY-SA - 20/09/2021 - Miguel Sevilla-Callejo                                            #
#  (revisando para Ubuntu 20.04.3)                                                           #
#                                                                                            #
##############################################################################################

#---------------------------------------------------------------------------------------------

# Si no se quiere instalar algún programa, comentar la línea en cuestión
# incluyendo el símbolo de almoadilla "#" al comienzo

# Asegurarse que el archivo está en modo de ejecución: $ chmod +x nombre_archivo.sh
# Después ejecutarlo con el comando "bash nombre_archivo.sh" ó "./nombre_archivo.sh"


### instalar programas para notificación de este script (si se corre desde GUI)

# zenity
sudo apt install -y zenity
# libnotify
sudo apt install -y libnotify-bin curl

# en KDE usar kdialog (está instalado por defecto)
#sudo apt install -y kdialog


#### mensajes de inicio
zenity --info --text="
<b>Script de instalación personalizado para Ubuntu 20.04 LTS bionic (y derivadas)</b>
(revisado para Ubuntu 20.04.3)

CC-BY-SA - 20 de septiembre de 2021
Miguel Sevilla-Callejo

[pulsa OK para comenzar]
" --width=300

# Para KDE se puede sustituir zenity con kdialog

#---------------------------------------------------------------------------------------------
# COMIENZO DE INSTALACIÓN
#---------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------
# ACTIVACIÓN DE REPOSITORIOS NUEVOS
#---------------------------------------------------------------------------------------------

notify-send "Activación de repositorios nuevos"

# Añadir los repositorios a usar (ver línea ejemplo para añadir repositorios).
# sudo add-apt-repository "repositorio PPA que se quiere añadir" (https://launchpad.net/)

# Se ha añadido un número para activar o desactivar la instalación correspondiente más abajo
# (si no se instalará una versión inferior o no podrá instalarse el programa)

# Los repositorios extras se incluirán en archivos independientes en /etc/apt/sources.list.d
# e.g. se crea el archivo /etc/apt/sources.list.d/cran.list para el repositorio de R.


# (0) Libreoffice - últimas versiones (7.x)
# actualiza la versión que ya viene instalada
sudo add-apt-repository ppa:libreoffice/ppa -y


# (1) QGIS Latest Release

# Se ha sustituido el repo de UbuntuGIS por el de QGIS
# https://wiki.ubuntu.com/UbuntuGIS

  # autorizar clave repositorio (es la misma en todos los casos) / actualizada a 20221108
wget -qO - https://qgis.org/downloads/qgis-2022.gpg.key | sudo gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/qgis-archive.gpg --import
sudo chmod a+r /etc/apt/trusted.gpg.d/qgis-archive.gpg

  # QGIS Latest Release
	# revisar si se instala en Linux Mint para sustituir `$(lsb_release -sc)` por `focal`
sudo sh -c 'echo "# QGIS Latest Release
deb [arch=amd64] http://qgis.org/ubuntu $(lsb_release -sc) main
deb-src http://qgis.org/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/qgis-latest.list'

  # QGIS Long Term Release
#sudo sh -c 'echo "# QGIS Latest Release
#deb [arch=amd64] http://qgis.org/ubuntu-ltr $(lsb_release -sc) main
#deb-src http://qgis.org/ubuntu-ltr $(lsb_release -sc) main" > /etc/apt/sources.list.d/qgis-ltr.list'

# (6) R - versión 4.x
# COMENTAR si se quiere la versión 3.6 que está en repo oficial de focal
# más info en: https://cran.r-project.org/bin/linux/ubuntu/
	# Añadir el repo e importar la clave de gpg de autorización
sudo sh -c 'echo "# R (CRAN project) repository for 4.x packages
deb https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/" > /etc/apt/sources.list.d/cran.list'

gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
gpg -a --export E298A3A825C0D65DFD57CBB651716619E084DAB9 | sudo apt-key add -

# (7) Virtualbox
# https://www.virtualbox.org/wiki/Linux_Downloads
	# Añadir el repositorio en /etc/apt/sources.list
sudo sh -c 'echo "# Virtualbox Repository
deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib" > /etc/apt/sources.list.d/virtualbox.list'
# importar la clave de gpg de autorización (actualizado y verificado 20170108)
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -


# (10) Google Earth
# en Linux Mint ya viene activada la fuente de Google
# se ha corregido para que coincida y no duplique: ...sources.list.d/google-earth.list)
sudo sh -c 'echo "# Google Earth (pro) Repository
deb [arch=amd64] http://dl.google.com/linux/earth/deb/ stable main" > /etc/apt/sources.list.d/google-earth-pro.list'
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -

# (11) Numix icons & themes
# https://numixproject.org/
sudo add-apt-repository -y ppa:numix/ppa

# (12) GIMP - GNU Image Manipulating Program
	# NO disponible en bionic --> hay que añadir ppa o por snap
	# https://tecadmin.net/install-gimp-on-ubuntu-20-04/
sudo add-apt-repository -y ppa:ubuntuhandbook1/gimp 

	# aconsejable PPA para tener los plug-ins actualizados -- POR REVISAR
	# https://www.howtoforge.com/tutorial/how-to-extend-gimp-with-gmic/
sudo add-apt-repository -y ppa:otto-kesselgulasch/gimp


# (13) Oracle Java JDK 9 (needed for GeoServer Web installer)
# http://www.webupd8.org/2015/02/install-oracle-java-9-in-ubuntu-linux.html
#sudo add-apt-repository -y ppa:webupd8team/java
# EN REVISIÓN, actualmente usando openjdk-11-jre

# (14) Ulauncher / sustituto de Synapse
# https://itsfoss.com/ulauncher/
#sudo add-apt-repository ppa:agornostal/ulauncher

# (15) Shutter --> se está usando Flameshot
# arreglar problemas con edición de imágenes en bionic:
# https://www.linuxuprising.com/2018/04/fix-shutter-edit-button-greyed-out-in.html
#sudo add-apt-repository ppa:linuxuprising/shutter -y

# (19) VSCodium / FOSS binaries of VSCode
# https://vscodium.com/ 
# REVISAR web -- NO carga la clave pública
#wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add - 
#echo 'deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list 

# (20) PostgreSQL + PostGIS + pgAdmin4
# https://installfights.blogspot.com/2018/11/how-to-install-pgadmin4-in-ubuntu-1810.html
#sudo apt install -y curl ca-certificates
#curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
#sudo sh -c 'echo "deb [arch=amd64] http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

# Instalar última versión de youtube-dl
sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl
sudo chmod a+rx /usr/local/bin/youtube-dl

# (21) Youtube-viewer --> REVISAR no funcionó a 20200902
# https://github.com/trizen/youtube-viewer
#sudo add-apt-repository -y ppa:nilarimogard/webupd8

# (22) Spotify
# https://www.spotify.com/us/download/linux/
# hay opción de instalación por paquete snap
curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | sudo apt-key add -
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list

# (23) Podman (alternativa a Docker)
# https://podman.io/getting-started/installation.html
echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_20.04/ /" | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
curl -L "https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_20.04/Release.key" | sudo apt-key add -

#---------------------------------------------------------------------------------------------
# FIN ACTIVACIÓN DE REPOSITORIOS NUEVOS
notify-send "Fin de la activación de repositorios nuevos."

#---------------------------------------------------------------------------------------------
# Activar socios repositorios de los socios de cannonical
# Necesario para instalar, por ejemplo, Skype <-- REVISAR ya no uso Skype
sudo sed -i "/^# deb .*partner/ s/^# //" /etc/apt/sources.list

#---------------------------------------------------------------------------------------------
#### Cambiar repositorio de Ubuntu por defecto (España) al de la universidad de Zaragoza

# sustituir este:  deb http://es.archive.ubuntu.com/ubuntu/
# por este otro:  deb http://softlibre.unizar.es/ubuntu/archive/

# copia de seguridad
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bk

# cambio de repositorio
sudo sed -i 's/es.archive.ubuntu.com\/ubuntu\//softlibre.unizar.es\/ubuntu\/archive\//g' /etc/apt/sources.list

#---------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------
# Revisión de los repositorios y actualización de paquetes previos
#---------------------------------------------------------------------------------------------
notify-send  "Revisando los repositorios y actualizando paquetes preexistentes"

sudo apt update
sudo apt upgrade -y

#---------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------
# INSTALACIÓN DE NUEVOS PROGRAMAS
#---------------------------------------------------------------------------------------------
notify-send "
INSTALACIÓN DE NUEVOS PROGRAMAS
Muestra atención a los posibles mensajes emergentes
"

# Se ha incluido un número entre paréntesis para aquellos programas
# que se descargarán desde repositorios extras o especiales (cifrados más arriba).
# Desactivar acorde con la desactivación de más arriba.

# Si ya están instalados no lo instalará y seguirá el proceso adelante

# Se han agrupado los paquetes/programas por temáticas

#---------------------------------------------------------------------------------------------
# HERRAMIENTAS GENERALES
#---------------------------------------------------------------------------------------------
notify-send "Instalando herramientas generales y de configuración del sistema"

# Ubuntu Extras
# https://packages.ubuntu.com/focal/ubuntu-restricted-extras
# salta pantalla para aceptar EULA de fuentes de MS y da un error un URL
#sudo apt install -y ubuntu-restricted-extras

## Ayuda e interfaz en ESPAÑOL es_ES // por defecto en inglés en_US

# comandos para instalar ayuda e interfaz en español (desactivado por defecto)
# y pasar de LANG="en_US.UTF-8" a LANG="es_ES.UTF-8" en /etc/defaul/locale

#sudo apt install -y language-pack-es* manpages-es*
#sudo sed -i 's/en_US.UTF-8/es_ES.UTF-8/g' /etc/defaul/locale

# Synaptic - Manejo de paquetes por defecto en debian
sudo apt install -y synaptic

# Menulibre - Editor del menu de aplicaciones
sudo apt install -y menulibre

# Conky
# light-weight system monitor for X - http://conky.sourceforge.net/
sudo apt install -y conky-all lm-sensors hddtemp

# Tree -- verestructura de directorios
sudo apt install -y tree

# UFW - Firewall (por defecto en Linux Mint) y su GUI
# https://en.wikipedia.org/wiki/Uncomplicated_Firewall
sudo apt install -y ufw
	# GTk GUI de UFW
sudo apt install -y gufw
	# Qt GUI para KDE
#sudo apt install -y ufw-kde

#  (**) NTFS config - para configurar automáticamente las entradas a las particiones de Windows / DATOS compartidos
# NO es necesario si hacemos configuración manual en fstab - Recomendado para principiantes
#sudo apt install -y ntfs-config

# Configuración de la hora por internet

# en KDE neon parece que no está ntp y ntpdate
# >> timedatectl a sustituido a ntp en últimas veriones de ubuntu
# https://help.ubuntu.com/lts/serverguide/NTP.html#timedatectl

  # ntp --> a reemplazar
#sudo apt install -y ntp ntpdate

  # configurar la hora con el servidor ROA.es (recomendado para dentro del CSIC)
  # en /etc/default/ntpdate sustituir "ntp.ubuntu.es" por "hora.roa.es"
  # realizo copia de seguridad
#sudo cp /etc/default/ntpdate /etc/default/ntpdate.bkp
  # realizo cambio de servidor
#sudo sed -i 's/ntp.ubuntu.com/hora.roa.es/g' /etc/default/ntpdate

# https://wiki.archlinux.org/index.php/Systemd-timesyncd
# configurar servidor hora.roa.es
#sudo cp /etc/systemd/timesyncd.conf /etc/systemd/timesyncd.conf.bk
# este paso no funciona - REVISAR
#sudo echo"NTP=hora.roa.es
#FallbackNTP=hora.roa.es" >> /etc/systemd/timesyncd.conf

# (11b) Numix icons & themes
  # GTK theme
sudo apt install -y numix-gtk-theme
  # icon themes
sudo apt install -y numix-icon-theme*
  # KDE theme
#sudo apt install -y numix-kde-theme # revisar
  # Utilidades
sudo apt install -y numix-folders

# paquete requerido para Multi-Core System Monitor 1.45 en Cinnamon
# http://cinnamon-spices.linuxmint.com/applets/view/79
#sudo apt install gir1.2-gtop-2.0

# Interact with a EWMH/NetWM compatible X Window Manager
# para modificar ventanas
sudo apt install -y wmctrl

# GParted - para particionar el disco duro (no viene por defecto)
# más paquetes para operatr con fat32 y NTFS
sudo apt install -y gparted dosfstools ntfsprogs

# partitionmanager (alternativa KDE a Gparted)
#sudo apt install -y partitionmanager

# TLP -- Comentar si NO es un laptop!!
sudo apt install -y tlp

# Gnome System Log Viewer
# https://apps.ubuntu.com/cat/applications/precise/gnome-system-log/
sudo apt install -y gnome-system-log
## ATENCIÓN! en Mate es: mate-system-log
#sudo apt install -y mate-system-log

# Tweaks para Gnome
# Desactivas si se usa otro DE
	# Gnome Tweaks - 
	# https://github.com/GNOME/gnome-tweaks
	# https://itsfoss.com/gnome-tweak-tool/
sudo apt install -y gnome-tweaks

	# decof-editor - low-level configuration system GUI
	# https://wiki.gnome.org/Projects/dconf
sudo apt install -y dconf-editor

# Xclip - para enviar textos al portapapeles desde CLI
sudo apt install -y xclip

# para enviar email desde la consola -- REVISAR
# http://www.uroboros.es/raspi/trucos.html#5
#sudo apt install -y ssmtp mailutils

# Cryptsetup and LUKS - open-source disk encryption
sudo apt install -y cryptsetup

# CryFS - Cryptographic filesystem for the cloud
sudo apt install -y cryfs

# Sirikali - GUI application that manages cryfs... based encrypted folders
sudo apt install -y sirikali

#---------------------------------------------------------------------------------------------
# Herramientas de programación

# Geany y extensiones
# http://www.geany.org/
sudo apt install -y geany geany-plugins

# (19) VSCodium
# https://vscodium.com/
sudo apt install -y codium 

# Meld - Compara textos y directorios
# http://meldmerge.org/
sudo apt install -y meld

# (13) Oracle Java JDK 9
#sudo apt install -y oracle-java9-installer

# GIT
sudo apt install -y git-core

# Arduino y relacionados
#sudo apt install -y arduino fritzing


## Python (Python 2 obsoleto desde enero 2020)

	# Python3
sudo apt install -y python3 python3-pip jupyter #ipython3

	# Python entorno virtual
sudo apt install -y python3-virtualenv python3-venv

	# IDEs Python
	# Spyder3
#sudo apt install -y python3-spyder
	
	# PyCharm CE
#sudo snap install -y pycharm-community

	# Python Packages # REVISAR o instalar para usuario local vía pip
	# NumPy, SciPy, Pandas, Matplotlib, GeoPandas, GeoPy
#sudo apt install -y python3-{numpy,scipy,pandas,matplotlib,geopandas,geopy}

# Node.js + Node Package Manager
# JavaScript runtime built on Chrome's V8 JavaScript engine
# desde repositorio para ubuntu 20.04
#curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh
#sudo bash nodesource_setup.sh
#rm nodesource_setup.sh
sudo apt install -y nodejs

#---------------------------------------------------------------------------------------------
# UTILIDADES
#---------------------------------------------------------------------------------------------
notify-send "Instalando utilidades"

# Gestión de archivos comprimidos
sudo apt install -y rar unace p7zip p7zip-full p7zip-rar unrar lzip arj sharutils mpack lzma lzop

# KeepassXC - Gestión de claves
# https://keepassxc.org/
sudo apt install -y keepassxc

# Synapse --> usar Uluncher
# http://www.webupd8.org/2010/11/syanpse-is-here-new-semantic-launcher.html
# preinstalado en Ubuntu MATE 16.04 / NO en 18.04 bionic
#sudo apt install -y synapse

# (14) Ulauncher
#sudo apt install -y ulauncher

# Plank, sustituto ligero de Docky
# preinstalado en Ubuntu MATE 16.04
#sudo apt install -y plank

# Deja-dup - Herramienta de backups por defecto en Ubuntu
# es mejor que la que viene por defecto en Linux Mint
#sudo apt install -y deja-dup

# restic - Backups done right!
# https://restic.net/
sudo apt install -y restic

# (7) Virtualbox
# Para virtualizar otros sistemas operativos (versión con acceso a USB)
sudo apt install -y virtualbox-6.1 # actualiza a la versión 6.1

# Wine - Para correr programas de Windows en Linux
sudo apt install -y wine winetricks

# GPrename -  GUI para renombrado masivo de ficheros
sudo apt install -y gprename

# Clam - Antivirus
# http://www.clamav.net/index.html
sudo apt install -y clamav clamtk

# SQLite - Servidor/ciente de bases de datos y GUI
# http://sqlite.org/
sudo apt install -y sqlite3 sqlitebrowser
# --> sustituir sqliteman por sqlitestudio

# Most - browse or page through a text file
sudo apt install -y most
	# para tener las páginas de man en color
	# supongo solo para user que corre script
	export PAGER='most'
	# ESTO hay que hacerlo en cada usuario, lo anterior solo configura para el superusuario

# Byobu - Powerful, text based window manager and shell multiplexer
# http://byobu.co/
sudo apt install -y byobu

# GNU Midnight Commander - a text-mode full-screen file manager
# http://www.midnight-commander.org/
sudo apt install -y mc

# pv - Shell pipeline element to meter data passing through
sudo apt install -y pv

# htop - an interactive process viewer (CLI)
sudo apt install -y htop

# PowerTop - tool to diagnose issues with power consumption and power management
# https://01.org/powertop/
sudo apt install -y powertop

# Configuración de compositor compiz (no está instalado por defecto en Ubuntu MATE)
# desactivo para Ubuntu
#sudo apt install -y compizconfig-settings-manager

# ppa-purge - Disables a PPA and reverts to the official packages if applicable
## --> sudo apt add-apt-repository -r - Will only remove the said PPA. Will not revert any packages.
## https://itsfoss.com/how-to-remove-or-delete-ppas-quick-tip/
sudo apt install -y ppa-purge

#---------------------------------------------------------------------------------------------
# CIENCIA E INVESTIGACIÓN
#---------------------------------------------------------------------------------------------
notify-send "Instalando paquetes de ciencia e investigación"

# SAGA GIS (ver. 2.3.1 disponible en repo de bionic)
sudo apt install -y saga

# (2) QGIS, GRASS y dependencias
sudo apt install -y qgis grass python-qgis qgis-plugin-grass grass-doc

# geotiff-bin: listgeo (dumps the metadata of a GeoTIFF file) & geotifcp (applies metadata).
sudo apt install -y geotiff-bin

# SpatiaLite y relacionados
# http://www.gaia-gis.it/gaia-sins/
  # Spatialite - información vectorial dentro de SQLite (libspatialite3 en precise)
sudo apt install -y libspatialite5 spatialite-bin
  # Rasterlite - información raster dentro de SQLite (librasterlite1 en precise)
sudo apt install -y librasterlite2 libgeotiff2 rasterlite-bin
  # FreeXL - permite conectar con ficheros Excel
sudo apt install -y libfreexl1
  # GUI para SpatiaLite (depende de las anteriores, creo)
sudo apt install -y spatialite-gui

# QLandKarte - gestión de datos GPS + plug-ins para GARMIN
# http://www.qlandkarte.org/ DISCONTINUADO
#sudo apt install -y qlandkartegt

# alternativa: QMapShack -- REVISAR
sudo apt install -y qmapshack

# GPSBabel (administración y manejo de datos GPS) e interfaz gráfico Qt (gpsbabelfe)
# http://www.gpsbabel.org/
sudo apt install -y gpsbabel gpsbabel-gui

# GARMIN GPS browser plug-in
# http://www.andreas-diesner.de/garminplugin/doku.php
sudo apt install -y garmin-plugin

# (10) Google Earth (pro)
sudo apt install -y google-earth-pro-stable

# Marble - Alternativa libre a Google Earth
# http://marble-globe.org/
sudo apt install -y marble

# (6a) Núcleo de R (http://cran.es.r-project.org/)
sudo apt install -y r-base r-base-dev r-base-html build-essential

# dependencias que me han surgido para instalar paquetes:					# nombrepaquete

sudo apt install -y make libcurl4-openssl-dev libssl-dev pandoc libxml2-dev # tydiverse
sudo apt install -y libmariadbclient-dev libssl-dev libsodium-dev 			# openssl
sudo apt install -y libgdal-dev libproj-dev build-essential 				# gdal
sudo apt install -y libudunits2-dev libgdal-dev libgeos-dev libproj-dev 	# sf
sudo apt install -y protobuf-compiler libprotobuf-dev 						# tmap
sudo apt install -y default-jdk 						# rJava (it's needed for some packages)
sudo apt install -y libpq-dev 												# RPostgreSQL
sudo apt install -y libgmp-dev libmpfr-dev 									# HH

# NO instalar los paquetes de R listados en repositorio de Ubuntu o desde
# repo de R extra pues solo se mantienen actualizados unos pocos (ver abajo).

# Paquetes mantenidos en repo extra -- REVISAR
# ver más info en: https://cran.r-project.org/bin/linux/ubuntu/
#sudo apt install -y r-cran-boot r-cran-class r-cran-cluster \
#r-cran-codetools r-cran-foreign r-cran-kernsmooth r-cran-lattice \
#r-cran-mass r-cran-matrix r-cran-mgcv r-cran-nlme r-cran-nnet \
#r-cran-rpart r-cran-spatial r-cran-survival

# solo el siguiente NO se instala con la base de más arriba
#sudo apt install -y r-cran-rodbc # -- REVISAR

# R Commander (y sus dependencias) -- Instalar desde R
#sudo apt install -y r-cran-rcmdr


#---------------------------------------------------------------------------------------------
# DISEÑO GRÁFICO
#---------------------------------------------------------------------------------------------
notify-send "Instalando programas de diseño gráfico"

# Imagemagick
# https://imagemagick.org/
sudo apt install -y imagemagick

# Editor de graficos vectoriales Inkscape
# http://inkscape.org/?lang=es
# extra para convertir en diferentes tipos de archivos
sudo apt install -y inkscape

# (12) GIMP - GNU Image Manipulator Program - El "Photoshop de Linux"p
# http://www.gimp.org
sudo apt install -y gimp
sudo apt install -y gimp-plugin-registry gimp-lensfun gimp-gmic # comentar si no se usa repositorio

# Krita - KDE image editor
#sudo apt install -y krita

# KIPI - KDE Image Plugin Interface
#sudo apt install -y kipi-plugins

# Darktable
sudo apt install -y darktable

# (15) Shutter - capturas de pantalla --> usar Flameshot
# y paquetes (libgoo-canvas-perl) para activar edición dentro de shutter
#sudo apt install -y shutter libgoo-canvas-perl

# Flameshot
sudo apt install -y flameshot

# KDE spectacle - instalado en KDE

# Blender - Edición en 3D - Activar si se usa edición 3D
# http://blender.org
#sudo apt install -y blender

# gThumb - Visor de imágenes (por defecto en Linux Mint)
#sudo apt install -y gthumb

# Edición, manejo y creación de archivos de fuentes
	# Fontforge --> mejor usar birdfont
	# https://fontforge.github.io/
#sudo apt install -y fontforge

	# Birdfont
sudo apt install -y birdfont

# Manejo de fuentes instaladas
	# Font Manager
#sudo apt install -y font-manager
	# Font Matrix
sudo apt install -y fontmatrix

# Agave - generate a variety of colorschemes from a single starting color
# http://home.gna.org/colorscheme/
#sudo apt install -y agave

# GPick - an advanced color picker
# http://www.gpick.org/
sudo apt install -y gpick


#-----------------------------------------------------------------------------------------
# EDICIÓN DE AUDIO Y VIDEO
#-----------------------------------------------------------------------------------------
notify-send "Instalando programas de audio y video"

# VLC (por defecto en Mint)
# http://www.videolan.org/vlc/
sudo apt install -y vlc

# OBS Studio
sudo apt install -y obs-studio

# Openshot - Sencilla edición de vídeo
# http://www.openshotvideo.com/
sudo apt install -y openshot
	# filtros extras / minimalistic plugin API for video effects, plugins collection
sudo apt install -y frei0r-plugins

# KDEnlive
#sudo apt install -y kdenlive

# Transformación de vídeo / archivos multimedia
	# Handbrake
	# http://handbrake.fr/
sudo apt install -y handbrake
	# WinFF -- REVISAR / Sustituir
#sudo apt install -y winff

# Audacity - Editor de audio
# http://audacity.sourceforge.net/
sudo apt install -y audacity

# Sound Converter - convert sound files to other formats (+ lame --> MP3)
# http://soundconverter.org/
sudo apt install -y soundconverter lame

# Peek - Captura de pantalla a GIF
sudo apt install -y peek

#(21) Youtube-viewer --> REVISAR
#sudo apt install -y youtube-viewer

# (22) Spotify
sudo apt install -y spotify-client
#snap install spotify # --> esa sería la forma de instalarlo por snap

# Asunder - An application to save tracks from an Audio CD as WAV, MP3, OGG, FLAC, and/or Wavpa
#sudo apt install -y asunder
# gestión óptima de compresión. probando

# Sound Juicer - A CD ripper for GNOME which aims to have a simple, clean, easy to use interface.
# NO permite cambiar configuración de MP3
# --> ver: http://catlingmindswipe.blogspot.com.es/2012/11/how-to-change-sound-juicer-and.html
#sudo apt install -y sound-juicer

# EasyMP3 Gain
# para normalizar archivos de audio
# http://sourceforge.net/projects/easymp3gain/
#sudo apt install -y easymp3gain-gtk

# EasyTAG - editor etiquetas archivos de audio
# http://easytag.sourceforge.net/
#sudo apt install -y easytag

# Radiotray - Pequeña aplicación para escuchar radios online
# http://radiotray.sourceforge.net/
#sudo apt install -y radiotray

# Play & copy encrypted DVDs with CSS (the Content Scramble System)
	# Revisado para Xential / Salta una ventana de configuración
#sudo apt install -y libdvd-pkg
	# antiguo código
#sudo apt install -y libdvdread4 # esto se instala
#sudo /usr/share/doc/libdvdread4/install-css.sh # esto ya no funciona

# queda revisar si se instlala libdvdcss2
# Revisar: https://itsfoss.com/play-dvd-ubuntu-1310/


#-----------------------------------------------------------------------------------------
# HERRAMIENTAS DE INTERNET
#-----------------------------------------------------------------------------------------
notify-send "Instalando programas de Internet"


# Thunderbird - Por defecto en Ubuntu
#sudo apt install -y thunderbird #thunderbird-locale-es

# Chromium - versión libre de Chrome de Google
# desde focal se instala la versión de snap
sudo apt install -y chromium-browser

# FTP Filezilla
# http://filezilla-project.org/
sudo apt install -y filezilla

# nmap - scanea los puertos
# sudo nmap -sS -O 127.0.0.1 # para saber que puertos están abiertos
# http://nmap.org/
sudo apt install -y nmap

# curl - command line tool for transferring data with URL syntax
# http://curl.haxx.se/
sudo apt install -y curl

# Cliente de NextCloud
sudo apt install -y nextcloud-desktop

# Zeroconf - Automatic local network configuration and discovery for Debian
# viene preinstalado en Ubuntu MATE
#sudo apt install -y avahi-daemon avahi-discover libnss-mdns

# lftp - CLI FTP avanzado - sophisticated ftp/http client
# http://lftp.yar.ru/
#sudo apt install -y lftp

# Remmina - Visualizador de escritorios remotos (y plug-ins)
# viene preinstalado en Ubuntu
#sudo apt install -y remmina remmina-plugin-*

# KDE --> KRDC
#sudo apt install -y krdc

#-----------------------------------------------------------------------------------------
# HERRAMIENTAS DE OFIMÁTICA
#-----------------------------------------------------------------------------------------
notify-send "Instalando herramientas de ofimática"

# (0) Libreoffice
# en el paso anterior debería de haberse instalado a una versión más moderna (> 4.x)
# pero los paquetes de en español no están incluidos por defecto en instalación en inglés

	# instalar paquetes básicos de libreoffice
	# no están por defecto en KDE Neon o en instalación básica de Ubuntu Mate 18.04
sudo apt install -y libreoffice

	# igualmente es posible que Base no esté instalado
sudo apt install -y libreoffice-base libreoffice-l10n-es libreoffice-help-es
	# tema de iconos extra (cambiar en Herramientas>Opciones>Vista>Iconos...)
#sudo apt install -y libreoffice-style-sifr # el tema Breeze viene por defecto y está bien
	# driver para hsqlbd & postgresql
sudo apt install -y libreoffice-sdbc-hsqldb libreoffice-sdbc-postgresql

# instalar el diccionario en español de Aspell
# https://en.wikipedia.org/wiki/GNU_Aspell
# CREO que se instala con Libreoffice, entre otros > desactivado por defecto
sudo apt install -y aspell-es

# unixODBC
# http://www.unixodbc.org/
# ODBC (Open Database Connectivity) is a standard programming language middleware API for accessing database management systems (DBMS)
sudo apt install -y unixodbc unixodbc-bin

# SQLite driver for ODBC
sudo apt install -y libsqliteodbc

# Para usar SQLite en LibreofficeBase a través de ODBC
# Fuente: https://wiki.openoffice.org/wiki/Documentation/How_Tos/Using_SQLite_With_OpenOffice.org
# Ver notas personales independientes de este script


# KDE --> Okular
# Okular - Document Viewer
# visor PDFs con capacidad de anotación (desde ver > 0.5 NO precise)
# extras to view CHM, TIFF, DjVu files, EPub eBooks & OPD presentations
#sudo apt install -y okular okular-extra-backends okular-backend-odp

# pdfshuffler
# editor de PDFs
sudo apt install -y pdfshuffler

# PDFToolkit - simple tool for doing everyday things with PDF documents
# https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/
# https://www.linux.com/learn/manipulating-pdfs-pdf-toolkit
sudo apt install pdftk -y

# Editor PDF -- REVISAR
# https://launchpad.net/updf
#sudo apt install -y updf

# Krop -- cortar PDF
sudo apt install -y krop

# Scribus publishing
# http://www.scribus.net/canvas/Scribus
#sudo apt install -y scribus

# Pandoc - library for converting from one markup format to another
#sudo apt install -y pandoc
	# Actualizado a versión 2.x desde GitHub
url_pandoc="https://github.com/jgm/pandoc/releases/download/2.14.2/pandoc-2.14.2-1-amd64.deb"
wget $url_pandoc -O /tmp/pandoc.deb
sudo dpkg -i /tmp/pandoc.deb

# Herramientas DjvuLibre
# http://djvu.sourceforge.net/
sudo apt install -y pdf2djvu djvulibre-desktop

# Scan Tailor - an interactive post-processing tool for scanned pages
# http://scantailor.org/
# DESCONTINUADO
#sudo apt install -y scantailor

# MDB Tools -- REVISAR
# Para leer archivos mdb (MS Access) en Linux -- Ahora son accdb y no funciona
#sudo apt install -y mdbtools #mdbtools-gmdb  # este último no funciona

# Gnome MDB viewer - NO está disponible en bionic
# https://reposcope.com/package/mdbtools-gmdb
# Da un ERROR de dependencias no cumplidas al intentar instalar mdbtools-gmdb
#wget http://ftp.debian.org/debian/pool/main/m/mdbtools/mdbtools-gmdb_0.7.1-5_amd64.deb -O /tmp/mdbtools-gmdb_0.7.1-5_amd64.deb
#sudo dpkg -i /tmp/mdbtools-gmdb_0.7.1-5_amd64.deb

# Calibre - Gestor de libros electrónicos y lector
# https://calibre-ebook.com/download_linux
sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.py | sudo python -c "import sys; main=lambda:sys.stderr.write('Download failed\n'); exec(sys.stdin.read()); main()"

# FBReader - Gestor de libros electrónicos y lector
#sudo apt install -y fbreader

# BlueGriffon - Editor HTML  (heredero de Kompozer)
	# http://bluegriffon.org/
#url_bluegriffon="http://bluegriffon.org/freshmeat/3.0.1/bluegriffon-3.0.1.Ubuntu16.04-x86_64.deb"
#wget $url_bluegriffon -O /tmp/bluegriffon.deb
#sudo dpkg -i /tmp/bluegriffon.deb

# Lyx - WYSIWYG "LaTex" editor
# http://www.lyx.org/
#sudo apt install -y lyx

# Texlive - distribution for the TeX typesetting system that
# includes major TeX-related programs, macro packages, and fonts
# https://www.tug.org/texlive/

	# instalación completa: TODO texlive --> cerca de 3Gbs de instalación / comentada por defecto
#sudo apt install -y texlive-full

	# instalación parcial: base y algúnos extras (latex y fuentes)
	# paquete para lenguaje español
sudo apt install -y texlive texlive-lang-spanish texlive-fonts-extra texlive-latex-extra

# TextStudio - an integrated writing environment for creating LaTeX documents
# https://www.texstudio.org/
sudo apt install -y texstudio


# Gestor de tiempos pomodoro de Gnome
sudo apt install -y gnome-shell-pomodoro

#-----------------------------------------------------------------------------------------
# SERVICIOS (SSH, Bases de datos, Web, etc)
#-----------------------------------------------------------------------------------------
notify-send "Instalando paquetes de diversos servicios"

# Servidor SSH
# para poder conectarse por el protocolo SSH al ordenador
sudo apt install -y ssh

# SSH file system - Para montar directorios remotos vía SSH
sudo apt install -y sshfs

# CIFS - para montar carpetas samba
sudo apt install -y cifs-utils

# Docker - Para correr containers de servicios
# https://www.docker.com/why-docker
sudo apt install -y docker.io

# (23) Podman
sudo apt-get -y install podman

# (20) PostgreSQL + PostGIS + pgAdmin4
# Aunuqe está disponible en repo bionic las últimas versiones (incluida pgadmin4) es mejor por repo propia.
#sudo apt install -y postgresql postgis
#sudo apt install -y pgadmin4

# MySecureShell - MUY ÚTIL PARA gestión cuentas SFTP
#sudo apt install -y mysecureshell

# Servidores web --> mejor usar vía Docker

# Apache + Tomcat
#sudo apt install -y apache2 tomcat9

# Nginx
#sudo apt install -y nginx


#-----------------------------------------------------------------------------------------
# EXTRAS
#-----------------------------------------------------------------------------------------
# aplicaciones sin repositorio o a actualizar

## (6b) RStudio
	# web de descarga:
	# http://www.rstudio.com/products/rstudio/download/
	# existe un PPA pero no es oficial
	# REVISAR

# versión 1.4.1717 (actualizado a 20210902)
wget https://download1.rstudio.org/desktop/bionic/amd64/rstudio-1.4.1717-amd64.deb -O /tmp/rstudio.deb
sudo dpkg -i /tmp/rstudio.deb

  # dependencia de RStudio (antiguo, revisar)
#sudo apt install -y libjpeg62
  # descargar paquete e instalar
  #wget "http://download1.rstudio.org/rstudio-0.98.490-$ARCBITS.deb"
  #sudo dpkg -i rstudio-0.98.490-$ARCBITS.deb

## Rclone - actualización - en focal está la 1.50.2
wget -O /tmp/rclone.deb https://downloads.rclone.org/rclone-current-linux-amd64.deb 
dpkg -i -y /tmp/rclone.deb
rm /tmp/rclone.deb

## Restic - actualización - en focal está la 0.9
# versión 0.12.0 a 20210902
wget -O /tmp/restic.bz2 https://github.com/restic/restic/releases/download/v0.12.0/restic_0.12.0_linux_amd64.bz2
cd /tmp
bzip2 -d restic.bz2
sudo chmod +x restic
sudo mv /tmp/restic /usr/bin/restic

# Freetube - The Private YouTube Client
# https://freetubeapp.io/
wget https://github.com/FreeTubeApp/FreeTube/releases/download/v0.13.2-beta/freetube_0.13.2_amd64.deb -O /tmp/freetube.deb
sudo dpkg -i /tmp/freetube.deb

#-----------------------------------------------------------------------------------------

# REVISAR

## WPS Office for Linux - Clon de MS Office con muy buena compatibilidad
	# web de descarga:
	# http://wps-community.org/downloads

#-----------------------------------------------------------------------------------------
## En aplicaciones sin instalación

## SQLiteStudio -- binario ejecutable (sustituto de sqliteman)
	# web de descarga:
	# https://sqlitestudio.pl/index.rvt?act=download

## Freefilesync --> descargar binario ejecutable
	# web de descarga:
	# https://www.freefilesync.org/download.php

## Tor Browser
	# web de descarga:
	# https://www.torproject.org/download/download-easy.html.en

## Otros: 
# DBeaver, JOSM, SNAP (teledetección), Telegram, Zotero


#-----------------------------------------------------------------------------------------
#Ahora, se limpia todo... ¡y listo!
#-----------------------------------------------------------------------------------------
notify-send "Limpiando el sistema"

sudo apt autoremove -y 
sudo apt -y clean

#-----------------------------------------------------------------------------------------
# mensaje FINAL en pantalla

notify-send " Enhorabuena $USER ¡INSTALACIÓN COMPLETA!"

# Comandos útiles a correr como usuario tras la instalación:

# usar most para ver man en color
# export PAGER='most'

# instalar oh-my-zsh
# sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"

# hacer zsh como shell predeterminado (no necesarios si se instala lo de arriba)
# chsh -s $(which zsh)