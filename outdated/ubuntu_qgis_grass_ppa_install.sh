#!/bin/bash

# Script para instalación de QGIS y GRASS en Ubuntu
# con repositorios de QGIS y dependencias UBUNTUGIS

# no era posible tener QGIS 3.2 con dependencias UbuntuGIS
# por ello había controlar si la versión de Ubuntu era bionic para continuar
# AHORA DESACTIVADO
#if [ $(lsb_release -sc) = "bionic" ]; then
#	echo -e "\nTu distro es bionic este escript NO te sirve\n"
#	exit
#fi

## OJO, en Linux Mint 17.x o elementary OS hay que sustituir
## la cadena "$(lsb_release -sc)" por "bionic"

# Instrucciones en la web:
# https://qgis.org/en/site/forusers/alldownloads.html#debian-ubuntu

########################################################################
# arreglar problemas de instalación previos #
sudo apt --fix-broken install -y


## ACTIVADA VERSIÓN LT

########################################################################
# añadir PPA UbuntuGIS-inestable
sudo add-apt-repository -y ppa:ubuntugis/ubuntugis-unstable
# https://qgis.org/ubuntugis-nightly


########################################################################
# autorizar clave repositorio (es la misma en todos los casos)

# autorizar clave repositorio QGIS (es la misma en todos los casos)
# actualizado (29-03-2021)

wget -qO - https://qgis.org/downloads/qgis-2020.gpg.key | sudo gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/qgis-archive.gpg --import
sudo chmod a+r /etc/apt/trusted.gpg.d/qgis-archive.gpg

# A continuación se incluye el código para las tres diferentes versiones
# del programa LTR, LR y en desarrollo

# Comenta o activa aquella versión que quieras instalar
# Por defecto está activada la instalación del a versión LTR


########################################################################
### QGIS Long Term Release
#sudo sh -c 'echo "# QGIS Long Term Release
#deb     http://qgis.org/ubuntugis-ltr $(lsb_release -sc) main
#deb-src http://qgis.org/ubuntugis-ltr $(lsb_release -sc) main" > /etc/apt/sources.list.d/qgis-ltr.list'


########################################################################
### QGIS Latest Release
sudo sh -c 'echo "# QGIS Latest Release
deb     http://qgis.org/ubuntugis $(lsb_release -sc) main
deb-src http://qgis.org/ubuntugis $(lsb_release -sc) main" > /etc/apt/sources.list.d/qgis-latest.list'


########################################################################
### QGIS Development Version - 3.11 / numeración impar
#sudo sh -c 'echo "# QGIS Latest Release
#deb     http://qgis.org/ubuntugis-nightly $(lsb_release -sc) main
#deb-src http://qgis.org/ubuntugis-nightly $(lsb_release -sc) main" > /etc/apt/sources.list.d/qgis-nightly.list'



########################################################################
# actualizar sistema
sudo apt update

# si ya estaba instalado QGIS hay que hacer dist-upgrade
sudo apt -y dist-upgrade


########################################################################
# instalar QGIS / si es que no estaba instalado antes

sudo apt install -y qgis python-qgis qgis-plugin-grass


########################################################################
# instalar documentación de GRASS
sudo apt -y install grass-doc

########################################################################
# instalar plugin GRASS (sin dependencias para OSGeoLive)
cd /tmp; rm *.deb
sudo apt download qgis-plugin-grass  # descargar paquete del plug-in
sudo dpkg --force-all -i *.deb        # forzar la instalación

# >> Código parcialmente incluido en script de postinstalación de Ubuntu
