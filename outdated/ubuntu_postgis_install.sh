#!/bin/bash

# Script para instalación de PostgreSQL 11 + PostGIS 2.5
# últimas versiones a mayo de 2019

# Fuente:
# https://installfights.blogspot.com/2018/11/how-to-install-pgadmin4-in-ubuntu-1810.html

sudo apt-get install curl ca-certificates
curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -


sudo sh -c 'echo "deb [arch=amd64] http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

# para xenial funciona cambiando nombre
#sudo sh -c 'echo "deb [arch=amd64] http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

# actualizar el sistema
sudo apt update && sudo apt upgrade -y

# instalar postgresql, postgis y pgadmin4
sudo apt install postgresql postgis pgadmin4 -y

#End of script




