#!/bin/bash

# -------------------------------------------------------------
# script instalación de programas extras en Debian / MX Linux
# CC-BY 2020 Miguel Sevilla-Callejo
# actualizado a 2021-10-18 para MX Linux 21 / Debian 11 "bulleye"
# -------------------------------------------------------------

# -------------------------------------------------------------
## AÑADIR REPOSITORIOS EXTRAS

sudo apt install -y software-properties-common

# [1] QGIS
  # some tools you will need for this instructions
sudo apt install -y gnupg software-properties-common
  # install the QGIS Signing Key
wget -qO - https://qgis.org/downloads/qgis-2021.gpg.key | sudo gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/qgis-archive.gpg --import
sudo chmod a+r /etc/apt/trusted.gpg.d/qgis-archive.gpg
  # add repository QGIS LR
#sudo add-apt-repository "deb https://qgis.org/debian `lsb_release -c -s` main"
echo 'deb https://qgis.org/debian bullseye main' | sudo tee --append /etc/apt/sources.list.d/qgis_lr.list
  # add repository QGIS LTR
#echo 'deb https://qgis.org/debian-ltr bullseye main' | sudo tee --append /etc/apt/sources.list.d/qgis_ltr.list

# [2] R
# sudo apt-key adv --keyserver keys.gnupg.net --recv-key 'E19F5F87128899B192B1A2C2AD5F960A256A04AF'
# da error la clave
wget -O /tmp/jranke.asc https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xe19f5f87128899b192b1a2c2ad5f960a256a04af
sudo apt-key add /tmp/jranke.asc
#sudo add-apt-repository "deb http://cloud.r-project.org/bin/linux/debian bullseye-cran40/"
echo 'deb http://cloud.r-project.org/bin/linux/debian bullseye-cran40/' | sudo tee --append /etc/apt/sources.list.d/cran40.list

# [3] VSCodium - NO hay versión actualizada 32 bits
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium.gpg
echo 'deb https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list

# [4] Spotify - Cambiado a testing
curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | sudo apt-key add - 
echo "deb http://repository.spotify.com testing non-free" | sudo tee /etc/apt/sources.list.d/spotify.list

# como alternativa CLI 32bits se puede usar ncspot: https://github.com/hrkfdn/ncspot
# para debian hay que compilar con rust

# [5] VirtualBox
echo "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian bullseye contrib" | sudo tee /etc/apt/sources.list.d/virtualbox.list
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -

# [6] Darktable - NO hay versión de 32bits - añadir alternativa
echo 'deb http://download.opensuse.org/repositories/graphics:/darktable/Debian_10/ /' | sudo tee /etc/apt/sources.list.d/graphics:darktable.list
curl -fsSL https://download.opensuse.org/repositories/graphics:darktable/Debian_10/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/graphics_darktable.gpg > /dev/null


# -------------------------------------------------------------
# ACTUALIZAR
sudo apt update
sudo apt upgrade -y

# -------------------------------------------------------------
# INSTALAR

# entorno de trabajo personalizado
sudo apt install -y zsh
sudo apt install -y plank
#sudo apt install -y synapse # mejor usar rofi
sudo apt install -y rofi
sudo apt install -y xfce4-appmenu-plugin appmenu-*

# navegadores extras
sudo apt install -y falkon chromium

# i3wm
sudo apt install -y i3 i3blocks suckless-tools
sudo apt install -y dmenu i3lock xbacklight feh
sudo apt install -y nitrogen rofi gucharmap

# otros
sudo apt install -y ssh               # secure shell
sudo apt install -y geany             # editor de textos
sudo apt install -y keepassxc         # gestor de contraseñas
sudo apt install -y sirikali cryfs    # para crear carpetas encriptadas
sudo apt install -y rclone            # accede y monta carpetas en la nube
sudo apt install -y restic            # backups encriptados y secuenciales
sudo apt install -y nextcloud-desktop # cliente de la nube libre
sudo apt install -y screenfetch       # 
sudo apt install -y pandoc            # se actualiza más adelante
sudo apt install -y xclip             # portapapeles X
sudo apt install -y meld              # comparar archivos
sudo apt install -y byobu             # terminales independientes
sudo apt install -y mc                # explorador CLI
sudo apt install -y powertop          # gestión de energía
sudo apt install -y zenity            # mostrar ventanas y notificaciones
sudo apt install -y youtube-dl smtube          # actualizar desde repo a última versión
sudo apt install -y inkscape          # editor vestorial
sudo apt install -y fonts-font-awesome  # fuente especial para tema zsh agnostic
sudo apt install -y krop              # cortar PDFs
sudo apt install -y remmina remmina-plugin*   # gestor de conexiones a escritorios remotos
sudo apt install -y flameshot         # capturador de pantalla
sudo apt install -y peek              # captura de pantalla en vídeo

#sudo apt install -y

# [1] QGIS & GRASS
sudo apt install -y qgis qgis-plugin-grass
sudo apt install -y grass grass-gui grass-doc
sudo apt install -y saga

# [2a] R 
sudo apt install -y r-base-core r-base-dev
sudo apt install -y r-base-html build-essential
  # dependencias que me han surgido para instalar paquetes:					        # nombrepaquete
  sudo apt install -y make libcurl4-openssl-dev libssl-dev pandoc libxml2-dev # tydiverse
  sudo apt install -y libmariadb-dev libssl-dev libsodium-dev 			  # openssl / libmariadbclient-dev
  sudo apt install -y libgdal-dev libproj-dev build-essential 				        # gdal
  sudo apt install -y libudunits2-dev libgdal-dev libgeos-dev libproj-dev 	  # sf
  sudo apt install -y protobuf-compiler libprotobuf-dev 						          # tmap
  sudo apt install -y default-jdk 						  # rJava (it's needed for some packages)
  sudo apt install -y libgmp-dev libmpfr-dev 									                # HH

# [3] VSCodium
sudo apt install -y codium

# [4] Spotify // NO funciona 32bits
sudo apt install -y spotify-client

# [5] Virtualbox
sudo apt install -y virtualbox-6.1

# [6] Darktable
sudo apt install -y darktable

# [2b] RStudio
	# descargar e instalar RStudio
wget -O /tmp/rstudio.deb https://download1.rstudio.org/desktop/bionic/amd64/rstudio-1.4.1717-amd64.deb
sudo dpkg -i /tmp/rstudio.deb

# Calibre
sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin

# actualizar pandoc
wget -O /tmp/pandoc.deb https://github.com/jgm/pandoc/releases/download/2.14.1/pandoc-2.14.1-1-amd64.deb
sudo dpkg -i /tmp/pandoc.deb

# actualizar rclone
  # 32 bits
#wget -O /tmp/rclone.deb https://downloads.rclone.org/rclone-current-linux-386.deb
  # 64 bits
wget -O /tmp/rclone.deb https://downloads.rclone.org/rclone-current-linux-amd64.deb
sudo dpkg -i /tmp/rclone.deb

# actualizar youtube-dl / REVISAR
sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl
sudo chmod a+rx /usr/local/bin/youtube-dl

# CLI Youtube Player: ytfzf
  # https://github.com/pystardust/ytfzf
  # dependencias
sudo apt install -y jq mpv youtube-dl fzf
sudo apt install -y libxext-dev python3.7-dev
pip3 install ueberzug
  # clonar repo e instalar
cd /tmp
git clone https://github.com/pystardust/ytfzf
cd ytfzf
sudo make install

# extensiones útiles zsh (correr como usuario)
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting