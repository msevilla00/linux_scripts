#!/bin/bash

# Ambient Noise - ANoise
# http://www.webupd8.org/2017/06/ambient-noise-anoise-player-fixed-for.html
# https://www.linuxadictos.com/trabaja-sin-distracciones-relajate-ayuda-ambient-noise-player.html/amp

sudo add-apt-repository ppa:costales/anoise -y

sudo apt update

sudo apt install -y anoise gir1.2-webkit-3.0 anoise-gui anoise-community-extension1 anoise-community-extension2 anoise-community-extension3 anoise-community-extension4 anoise-community-extension5
