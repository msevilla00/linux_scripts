#!/bin/bash

# Kodi
# http://kodi.wiki/view/HOW-TO:Install_Kodi_for_Linux

sudo apt install software-properties-common
sudo add-apt-repository ppa:team-xbmc/ppa -y
#sudo add-apt-repository ppa:team-xbmc/unstable -y 
#sudo add-apt-repository ppa:team-xbmc/xbmc-nightly -y
sudo apt-get update
sudo apt-get install kodi -y
