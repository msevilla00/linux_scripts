#!/bin/bash

# Script para instalación de QGIS y GRASS en Ubuntu
# ACTUALIZACIÓN Septiembre de 2021 para Ubuntu focal
# Sin dependencias UbuntuGIS

# Instrucciones en la web:
# https://qgis.org/en/site/forusers/alldownloads.html#debian-ubuntu


# Puede haber problemas con `$(lsb_release -sc)` por ello instalamos lsb_release
# también podemos cambiar eso por el texto `focal`
sudo apt install -y lsb_release 

## ACTIVADA VERSIÓN LT

########################################################################
# autorizar clave repositorio QGIS (es la misma en todos los casos)
# actualizado (septiembre de 2021)

wget -qO - https://qgis.org/downloads/qgis-2021.gpg.key | sudo gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/qgis-archive.gpg --import
sudo chmod a+r /etc/apt/trusted.gpg.d/qgis-archive.gpg

# A continuación se incluyen los repos para las tres diferentes versiones
# del programa LTR, LR y en desarrollo

# Comenta o activa aquella versión que quieras instalar
# Por defecto está activada la instalación del a versión LR


########################################################################
### QGIS Long Term Release

#sudo sh -c 'echo "# QGIS Long Term Release
#deb     http://qgis.org/ubuntu-ltr $(lsb_release -sc) main
#deb-src http://qgis.org/ubuntu-ltr $(lsb_release -sc) main" > /etc/apt/sources.list.d/qgis-ltr.list'


########################################################################
### QGIS Latest Release

sudo sh -c 'echo "# QGIS Latest Release
deb     http://qgis.org/ubuntu $(lsb_release -sc) main
deb-src http://qgis.org/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/qgis-latest.list'


########################################################################
### QGIS Development Version

#sudo sh -c 'echo "# QGIS Latest Release
#deb     http://qgis.org/ubuntu-nightly $(lsb_release -sc) main
#deb-src http://qgis.org/ubuntu-nightly $(lsb_release -sc) main" > /etc/apt/sources.list.d/qgis-nightly.list'


########################################################################
# actualizar sistema
sudo apt-get update

# si ya estaba instalado QGIS hay que hacer dist-upgrade
sudo apt-get -y dist-upgrade


########################################################################
# instalar QGIS, GRASS y SAGA

sudo apt install -y qgis grass saga python-qgis qgis-plugin-grass grass-doc

# SAGA GIS 2.3.1 está disponible en el repo oficial de Ubuntu bionic
  # Revisar esto para las últimas versiones de SAGA (compilar últimas versiones)
  # https://sourceforge.net/p/saga-gis/wiki/Binary%20Packages/#linux
  # NO ES NECESARIO (los algoritmos son los mismos de la versión 2.3.x)
