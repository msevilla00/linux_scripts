#!/bin/bash

# ----------------------------------------------------------------------
# script instalación de programas extras vía flatpack
# CC-BY 2023 Miguel Sevilla-Callejo
# actualizado a 2023-03-13 para Debian Testing "bookworm"
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# Instalación de flatpak y repositorio flathub / comentado por defecto

# sudo apt install -y flatpak
# sudo flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo

# ---------------------------------------------------------------------
# Instalación de paquetes

# ATENCIÓN: Si la instalación es para un solo usuario MEJOR usar 
# la opción de `flatpak install --user programa` y así se instalará en
# el directorio de usuario: `~/.local/share/flatpak`

# Hay que añadir el repositorio de flatpack a nuestro usuario
flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo


# Utilidades ----------------------------------------------------------
flatpak install --user -y flatseal     # gestión de permisos paquetes flatpak

flatpak install --user -y flathub io.dbeaver.DBeaverCommunity   # consultas bases BDs
flatpak install --user -y flathub org.freefilesync.FreeFileSync # sincron. directorios

# Extras KDE para complementar Gnome ---------------------------------
flatpak install --user -y flathub org.kde.kasts	# gestor de podcastc
flatpak install --user -y flathub org.kde.kdenlive	# editor de vídeo
flatpak install --user -y flathub org.kde.okular	# visor de documentos digitales (PDF, epub, etc.)

# IDEs & Co. ----------------------------------------------------------
flatpak install --user -y flathub com.vscodium.codium
flatpak install --user -y flathub org.zotero.Zotero


# Diseño y multimedia -------------------------------------------------

	# Interactive post-processing tool for scanned pages
flatpak install --user -y flathub com.github._4lex4.ScanTailor-Advanced

	# spotify & ncspot
flatpak install --user -y flathub io.github.hrkfdn.ncspot
flatpak install --user -y flathub com.spotify.Client


# Internet ------------------------------------------------------------

flatpak install --user -y flathub org.tribler.Tribler          # Torrents
flatpak install flathub io.webtorrent.WebTorrent        # Reproduce torrents
#flatpak install flathub org.onionshare.OnionShare      # en repo debian
flatpak install freetube

	# Videoconferencia y mensajería
flatpak install --user -y flathub org.signal.Signal       # Signal
flatpak install --user -y flathub us.zoom.Zoom            # Zoom
flatpak install --user -y flathub com.github.IsmaelMartinez.teams_for_linux # MS Teams
#flatpak install --user -y flathub org.telegram.desktop	
	# telegram está en repo testing de debian


# Juegos --------------------------------------------------------------
	# minetest (el de repo de debian daba problemas)
flatpak install --user -y flathub minetest

	# Steam con compatibilidad con juegos de windows (proton)
flatpak install --user -y flathub com.valvesoftware.Steam.CompatibilityTool.Proton com.valvesoftware.Steam.CompatibilityTool.Proton-Exp com.valvesoftware.Steam.CompatibilityTool.Proton-GE


exit

# Revisar - 20230313

flatpak install --user -y flathub com.github.tchx84.Flatseal
flatpak install --user -y flathub com.valvesoftware.Steam
flatpak install --user -y flathub io.dbeaver.DBeaverCommunity
flatpak install --user -y flathub io.freetubeapp.FreeTube
flatpak install --user -y flathub io.github.hrkfdn.ncspot
flatpak install --user -y flathub net.minetest.Minetest
flatpak install --user -y flathub net.xmind.XMind8
flatpak install --user -y flathub org.freefilesync.FreeFileSync
flatpak install --user -y flathub org.kde.kasts
flatpak install --user -y flathub org.kde.kdenlive
flatpak install --user -y flathub org.kde.okular
flatpak install --user -y flathub org.tribler.Tribler
flatpak install --user -y flathub org.zotero.Zotero
flatpak install --user -y flathub us.zoom.Zoom
flatpak install --user -y kopia

# BORRAR paquetes innecesarios:
flatpak uninstall --unused

# BORRAR TODOS los paquetes
flatpak unistall --all

