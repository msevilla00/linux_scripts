#!/bin/bash

# Paquetes a instalar en EndeavaourOS
# https://endeavouros.com/
# actualización a 20211114
# CC BY Miguel Sevilla-Callejo

# Instalando Xfce + i3wm desde inicio
# Si no se instala i3wm; listado de paquetes:
# https://raw.githubusercontent.com/endeavouros-team/EndeavourOS-packages-lists/master/i3
#sudo pacman -S --needed - < packages-repository.txt

# Creo un alias temporal
alias yinst="yay -S --noconfirm --needed"

#yinst `# añadir más apps`\

# herramientas del sistema -----------------------------------------------
yinst zsh lshw
yinst openssh
yinst htop byobu
yinst zenity
yinst rsync restic
yinst rclone
yinst rclone-browser
yinst cryfs sirikali-bin

# kernel lts
yinst linux-lts r8168-lts

# editores ---------------------------------------------------------------
yinst vim     	    # editor de textos CLI
#yinst emacs    	# editor de textos CLI
yinst geany    	    # editor de textos GUI
yinst vscodium-bin
yinst libreoffice-fresh libreoffice-fresh-es
yinst pandoc texlive-core
yinst evince 			# visor de PDF de gnome
yinst meld               # comparar archivos
yinst calibre
yinst zotero


# internet ---------------------------------------------------------------
yinst chromium			# navegador Chromium
yinst torbrowser-launches
yinst thunderbird
yinst telegram-desktop
yinst signal-desktop
yinst transmission-gtk
yinst nextcloud-client
yinst gnome-keyring 	# necesario para gestionar claves (nextcloud)
yinst popcorntime-bin
yinst remmina libvncserver freerdp

# diseño ----------------------------------------------------------------
yinst inkscape
yinst gimp
#yinst nspot 			# cliente de spotify CLI ligero 
yinst darktable
yinst flameshot
yinst peek
yinst cheese
yinst gucharmap 		# mapa de caracteres
#yinst fotowall         # desde AUR

# multimedia -----------------------------------------------------------
yinst spotify
yinst obs-studio		# grabación de pantalla
yinst ytfzfim fzf ueberzug # visor de youtube cl

# otras herramientas CLI -----------------------------------------------
yinst dua-cli 			# disk usage analyzer CLI

# otras herramientas GUI -----------------------------------------------
yinst keepassxc
yinst gnome-disk-utility
yinst docker docker-compose podman
yinst catfish
yinst blanket 			# sonido ambiente
yinst breaktimer-bin 	# temporizador

# juegos ---------------------------------------------------------------
yinst minetest

# análisis de datos ----------------------------------------------------

	# datos espaciales
yinst qgis
yinst josm
# yinst josm grass # compilado desde AUR

    # R
yinst r
yinst rstudio
yinst r-sf r-rgdal r-dplyr r-markdown

# ver documentación: https://wiki.archlinux.org/title/R
# repositorios de interés:
# 	https://wiki.archlinux.org/title/Unofficial_user_repositories#desolve
#	https://wiki.archlinux.org/title/Unofficial_user_repositories#rstudio

    # Python
yinst python-pip

    # bases de datos
yinst dbeaver

# virtualización -------------------------------------------------------

# tweaks ---------------------------------------------------------------
yinst papirus
yinst plank

# extras personales ----------------------------------------------------
yinst brother-hll2370dn
yinst autofirma-bin